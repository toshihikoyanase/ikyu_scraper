ikyu scraper
====

Downloader and scraper of ikyu.com for NLP research.

言語処理研究のためのikyu.comのダウンローダ＆スクレイパー。


## Requirement

* Python 2.7
* pip

## Install

```
pip install -r requirements.txt
```

## Usage

URLのリストを作る

* ikyu.comのホテルのクチコミのページネーションの最後のページを表示
* そのURLをテキストファイルurl_list.txtに1行1件で記録

各地域のホテル一覧ページからホテルのURLリストを抽出する
```
python download_hotel_urls.py etc/ikyu-area.txt | tee url_list.txt
```

HTMLファイルのダウンロードする
```
python dl.py url_list.txt html_dir
```

HTMLファイルからテキストとレーティングをスクレイプする
```
bash scrape_all.sh html_dir json_dir
```

## License

LICENSEに記したように、本コードのライセンスはCC0です。

These codes are licensed under CC0 as shown in LICENSE.

[![CC0](http://i.creativecommons.org/p/zero/1.0/88x31.png "CC0")](http://creativecommons.org/publicdomain/zero/1.0/deed.ja)
